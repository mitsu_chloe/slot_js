//title

function randomCharactor(c) {

  //跳ねさせる要素をすべて取得
  var randomChar = document.getElementsByClassName(c);

  //for で総当たり
  for (var i = 0; i < randomChar.length; i++) {

    //クロージャー
    (function(i) {

      //i 番目の要素、テキスト内容、文字列の長さを取得
      var randomCharI = randomChar[i];
      var randomCharIText = randomCharI.textContent;
      var randomCharLength = randomCharIText.length;

      //何番目の文字を跳ねさせるかをランダムで決める
      var Num = ~~(Math.random() * randomCharLength);

      //跳ねさせる文字を span タグで囲む、それ以外の文字と合わせて再び文字列を作る
      var newRandomChar = randomCharIText.substring(0, Num) + "<span>" + randomCharIText.charAt(Num) + "</span>" + randomCharIText.substring(Num + 1, randomCharLength);
      randomCharI.innerHTML = newRandomChar;

      //アニメーションが終わったら再び関数を発火させる
      document.getElementsByClassName(c)[0].children[0].addEventListener("animationend", function() {
          randomCharactor(c)
      }, false)
    })(i)
	}
}

//クラス名が pyonpyon のクラスを跳ねさせる
randomCharactor("pyonpyon");


//SLOT
var time1;
var time2;
var time3;

var value1;
var value2;
var value3;

// START
function start() {
    start1();
    start2();
    start3();
}
 
    //生成する文字列に含める文字セット
    var c = [
    "★",
    "☆",
    "♥",
    "○",
    "●",
    "■"
    ];

function congrats(){
    if(value1 === value2 &&  value2 === value3 && value1 === value3){
        window.alert('おめでとう');
    } else{
        window.alert('残念');
    }
}



//各ランダム値を表示
function start1() {
    document.getElementById("dat1").value = c[Math.floor(Math.random()*6)];
    time1 = setTimeout(start1,  10);
}
function start2() {
    document.getElementById("dat2").value = c[Math.floor(Math.random()*6)];
    time2 = setTimeout(start2,  10);
}
function start3() {
    document.getElementById("dat3").value = c[Math.floor(Math.random()*6)];
    time3 = setTimeout(start3,  10);
}

// STOP
var stop = 0;


function stop1() {
    clearTimeout(time1);
    stop++;
    value1 = document.getElementById("dat1").value;
    if(stop === 3){
        congrats();
    }
}
function stop2() {
    clearTimeout(time2);
    stop++;
    value2 = document.getElementById("dat2").value;
    if(stop === 3){
        congrats();
    }
}
function stop3() {
    clearTimeout(time3);
    stop++;
    value3 = document.getElementById("dat3").value;

    if(stop === 3){
        congrats();
    }
}

//resetボタン
function reset(){
  location.reload();
}